export const LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'USER_LOGIN_FAILURE';
export const LOGOUT = 'USER_LOGOUT';

export const GET_ALL_USERS = 'USER_GET_ALL';
export const REMOVE_USER = 'USER_REMOVE';
export const ADD_USER = 'USER_ADD';
export const EDIT_USER = 'USER_EDIT';
