export const GET_ALL_LESSONS = 'LESSONS_GET_ALL';
export const REMOVE_LESSON = 'LESSON_REMOVE';
export const ADD_LESSON = 'LESSON_ADD';
export const EDIT_LESSON = 'LESSON_EDIT';