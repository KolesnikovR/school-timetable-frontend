export const GET_ALL_CLASSES = 'CLASSES_GET_ALL';
export const REMOVE_CLASS = 'CLASS_REMOVE';
export const ADD_CLASS = 'CLASS_ADD';
export const EDIT_CLASS = 'CLASS_EDIT';
