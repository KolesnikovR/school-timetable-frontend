import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CircularProgress, Typography, IconButton } from '@material-ui/core';
import { DeleteOutline, Add, EditOutlined } from '@material-ui/icons';
import Dialog from '../../components/Dialog';
import Button from '../../components/Button';
import ClassesForm from './components/Form';
import {
    getClasses,
    createClass,
    editClass,
    deleteClass,
} from '../../actions/class';
import { USER_ROLE } from '../../consts';
import { useClassesStyles } from './styles';

const Classes = () => {
    const styles = useClassesStyles();
    const dispatch = useDispatch();
    const { classes } = useSelector(state => state.classData);
    const { user } = useSelector(state => state.authentication);
    const [isLoad, setIsLoad] = useState(true);
    const [isOpened, setIsOpened] = useState(false);
    const [deletedItemId, setDeletedItemId] = useState(null);
    const [editedItem, setEditedItem] = useState(null);
    const [isAddBtnClicked, setIsAddBtnClicked] = useState(false);

    const onCancel = () => {
        setIsAddBtnClicked(false);
        setEditedItem(null);
    }
    
    const handleDelete = (id) => {
        setIsOpened(true);
        setDeletedItemId(id);
    }

    const onDelete = async () => {
        await dispatch(deleteClass(deletedItemId));
        setIsOpened(false);
    };

    const onAdd = async (data) => {
        await dispatch(createClass(data));
        onCancel();
    };

    const onEdit = async (data) => {
        let updatedData = {};
        Object.keys(data).forEach(key => {
            if (data[key] !== editedItem[key]) {
                return updatedData[key] = data[key];
            }
        });
    
        await dispatch(editClass(editedItem.id, updatedData));
        onCancel();
    };

    useEffect(() => {
        dispatch(getClasses()).then(() => {
            setIsLoad(false);
        });
    }, [dispatch]);

    return (
        <div style={{ width: "100%" }}>
            {isLoad ? (
                <CircularProgress />
            ) : (
                <div>
                    {user.role === USER_ROLE.ADMIN && (
                        <Button
                            variant="contained"
                            color="primary"
                            size="large"
                            disabled={editedItem || isAddBtnClicked}
                            onClick={() => setIsAddBtnClicked(true)}
                        >
                            <Add />&nbsp;
                            Добавить класс
                        </Button>
                    )}
                    <div className={styles.list}>
                        {isAddBtnClicked && (
                            <div className={styles.item}>
                                <ClassesForm btnText="Добавить" onSubmit={onAdd} onCancel={onCancel} />
                            </div>
                        )}
                        {classes.sort((a, b) => a.name.localeCompare(b.name)).map(classData => (
                            <div className={styles.item} key={classData.id}>
                                {editedItem?.id === classData.id ? (
                                    <ClassesForm
                                        classData={classData}
                                        btnText="Добавить"
                                        onSubmit={onEdit}
                                        onCancel={onCancel}
                                    />
                                ) : (
                                    <>
                                        <div className={styles.itemHeading}>
                                            <Typography variant="h6">{classData.name}</Typography>
                                            {user.role === USER_ROLE.ADMIN && (
                                                <div>
                                                    <IconButton
                                                        disabled={editedItem || isAddBtnClicked}
                                                        onClick={() => setEditedItem(classData)}
                                                    >
                                                        <EditOutlined />
                                                    </IconButton>
                                                    <IconButton
                                                        disabled={editedItem || isAddBtnClicked}
                                                        onClick={() => handleDelete(classData.id)}
                                                    >
                                                        <DeleteOutline />
                                                    </IconButton>
                                                </div>
                                            )}
                                        </div>
                                    </>
                                )}
                            </div>
                        ))}
                    </div>
                </div>
            )}
            <Dialog
                isOpened={isOpened}
                handleClose={() => setIsOpened(false)}
                handleAdditionalButton={() => setIsOpened(false)}
                handlePrimaryButton={onDelete}
                primaryButtonText="Удалить"
                additionalButtonText="Отмена"
                variant="error"
            >
                <Typography variant="body1">Вы действительно хотите удалить класс?</Typography>
            </Dialog>
        </div>
    );
};

export default Classes;