import { makeStyles } from '@material-ui/core/styles';

export const useClassesStyles = makeStyles(theme => ({
    list: {
        display: 'grid',
        gridTemplateColumns: 'repeat(3, 1fr)',
        gridColumnGap: theme.spacing(2),
        gridRowGap: theme.spacing(2),
        width: '100%',
        height: '100%',
        marginTop: theme.spacing(2),
        boxSizing: 'border-box',
    },

    item: {
        width: '100%',
        minHeight: '135px',
        boxSizing: 'border-box',
        padding: theme.spacing(2),
        borderRadius: '10px',
        boxShadow: 'rgba(100, 100, 111, 0.2) 0px 7px 29px 0px',
    },

    itemHeading: {
        marginBottom: theme.spacing(1.5),
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    itemSubHeading: {
        marginBottom: theme.spacing(1),
    },
}));
