import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { FormTextField } from '../../../../components/inputs/TextField';
import Button from '../../../../components/Button';
import { useFormStyles } from './styles';
import { schema } from './schema';

const ClassesForm = ({ classData, btnText, onSubmit, onCancel }) => {
    const classes = useFormStyles();
    const [isSended, setIsSended] = useState(true);
    const { handleSubmit, formState: { errors }, control } = useForm({
        resolver: yupResolver(schema),
    });
    
    const submitForm = async (data) => {
        setIsSended(false);
        onSubmit(data).then(() => {
            setIsSended(true);
        });
    };

    return (
        <form onSubmit={handleSubmit(submitForm)} className={classes.root}>
            <FormTextField
                label="Название"
                control={control}
                errors={errors}
                name="name"
                defaultValue={classData?.name}
            />
            <Button
                type="submit"
                variant="contained"
                color="primary"
                size="medium"
                disabled={!isSended}
                isLoading={!isSended}
            >
                {btnText}
            </Button>
            <Button
                size="medium"
                onClick={onCancel}
                disabled={!isSended}
            >
                Отмена
            </Button>
        </form>
    );
};

export default ClassesForm;
