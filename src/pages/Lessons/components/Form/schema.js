import * as yup from 'yup';

export const schema = yup.object().shape({
    countPerWeek: yup.number().required().min(1),
});