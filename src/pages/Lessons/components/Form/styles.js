import { makeStyles } from '@material-ui/core/styles';

export const useFormStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2),

        '& .MuiFormControl-root': {
            margin: theme.spacing(2, 0),
        },
    },

    section: {
        width: '100%',
        height: '100%',
        padding: theme.spacing(3),
        boxSizing: 'border-box',
        boxShadow: '0 0 3px rgba(0, 0, 0, 0.5)',
        borderRadius: '10px',
    },
}));
