import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useForm } from 'react-hook-form';
import { useAsyncEffect } from 'use-async-effect';
import { CircularProgress } from '@material-ui/core';
import { yupResolver } from '@hookform/resolvers/yup';
import { FormTextField } from '../../../../components/inputs/TextField';
import { FormSelectField } from '../../../../components/inputs/Select';
import { getSubjects } from '../../../../actions/subjects';
import { getUsers } from '../../../../actions/user';
import { getClasses } from '../../../../actions/class';
import Button from '../../../../components/Button';
import { schema } from './schema';
import { useFormStyles } from './styles';

const LessonForm = ({ lesson, btnText, onSubmit, onCancel }) => {
    const dispatch = useDispatch();
    const styles = useFormStyles();
    const { subjects } = useSelector(state => state.subjectData);
    const { users } = useSelector(state => state.userData);
    const { classes } = useSelector(state => state.classData);
    const [isLoaded, setIsLoaded] = useState(false);
    const [isSended, setIsSended] = useState(true);
    const { formState: { errors }, control, watch, handleSubmit } = useForm({
        resolver: yupResolver(schema),
    });

    const watchSubject = watch('subject', lesson?.subject.id);
    const watchTeacher = watch('teacher', lesson?.teacher.id);
    
    const submitForm = async (data) => {
        await onSubmit({ ...data, countPerWeek: +data.countPerWeek });
        setIsSended(true);
    };

    useAsyncEffect(async () => {
        await dispatch(getSubjects());
        await dispatch(getUsers());
        await dispatch(getClasses());
        setIsLoaded(true);
    }, [dispatch]);

    if (!isLoaded) {
        return <CircularProgress />;
    }
    
    return (
        <form onSubmit={handleSubmit(submitForm)} className={styles.root}>
            <FormSelectField
                control={control}
                errors={errors}
                name="subject"
                label="Предмет"
                required
                data={
                    subjects
                        .filter(it => 
                            (!watchTeacher && !lesson?.teacher.id) || it.teachers.filter(t => t.id === (watchTeacher || lesson?.teacher.id)).length
                        )
                        .map(it => ({ id: it.id, value: it.name }))
                }
                defaultValue={lesson?.subject.id}
            />
            <FormSelectField
                control={control}
                errors={errors}
                name="teacher"
                label="Преподаватель"
                required
                data={
                    users
                        .filter(it => 
                            (!watchSubject && !lesson?.subject.id) || it.subjects.filter(t => t.id === (watchSubject || lesson?.subject.id)).length
                        )
                        .map(it => ({ id: it.id, value: `${it.surname} ${it.name[0]}. ${it.patronymic[0]}.` }))
                }
                defaultValue={lesson?.teacher.id}
            />
            <FormSelectField
                control={control}
                errors={errors}
                name="classData"
                label="Класс"
                required
                data={classes.map(it => ({ id: it.id, value: it.name }))}
                defaultValue={lesson?.classData.id}
            />
            <FormTextField
                type="number"
                label="Уроков в неделю"
                control={control}
                errors={errors}
                name="countPerWeek"
                required
                defaultValue={lesson?.countPerWeek}
            />
            <Button
                type="submit"
                variant="contained"
                color="primary"
                size="medium"
                disabled={!isSended}
                isLoading={!isSended}
            >
                {btnText}
            </Button>
            <Button
                size="medium"
                onClick={onCancel}
                disabled={!isSended}
            >
                Отмена
            </Button>
        </form>
    );
};

export default LessonForm;
