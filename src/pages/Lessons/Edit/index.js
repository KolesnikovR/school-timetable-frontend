import React, { useState } from 'react';
import { useAsyncEffect } from 'use-async-effect';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import { CircularProgress, Typography } from '@material-ui/core';
import { editLesson } from '../../../actions/lessons';
import LessonForm from '../components/Form';
import routeNames from '../../../consts/routeNames';
import { USER_ROLE } from '../../../consts';
import { getLessonById } from '../../../shared/api/rest/lessons';

const AddLesson = () => {
    const history = useHistory();
    let { id: lessonId } = useParams();
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.authentication);
    const [lesson, setLesson] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    if (user.role !== USER_ROLE.ADMIN) {
        history.push(routeNames.LESSONS.INDEX);
    }

    useAsyncEffect(async () => {
        getLessonById(lessonId).then(({ data }) => {
            setLesson(data);
            setIsLoaded(true);
        });
    }, []);

    const onSubmit = async (data) => {
        let updatedData = {};
        Object.keys(data).forEach(key => {
            if (data[key] !== (lesson[key]?.id || lesson[key])) {
                return updatedData[key] = data[key];
            }
        });

        dispatch(editLesson(lessonId, updatedData)).then(() => {
            history.push(routeNames.LESSONS.INDEX);
        });
    }

    if (!isLoaded) {
        return <CircularProgress />;
    }

    return (
        <div>
            <Typography variant="h4">Редактировать урок</Typography>
            <LessonForm lesson={lesson} btnText="Изменить" onSubmit={onSubmit} />
        </div>
    )
};

export default AddLesson;
