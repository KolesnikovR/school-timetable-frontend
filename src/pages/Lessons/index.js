import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { generatePath, useHistory } from 'react-router';
import { CircularProgress, Typography } from '@material-ui/core';
import { DeleteOutline, Add, EditOutlined } from '@material-ui/icons';
import { getLessons, deleteLesson } from '../../actions/lessons';
import Table from '../../components/Table';
import Dialog from '../../components/Dialog';
import routeNames from '../../consts/routeNames';
import { USER_ROLE } from '../../consts';

const Lessons = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { lessons } = useSelector(state => state.lessonsData);
    const { user } = useSelector(state => state.authentication);
    const [isLoad, setIsLoad] = useState(true);
    const [isOpened, setIsOpened] = useState(false);
    const [deletedItemId, setDeletedItemId] = useState(false);
    
    const handleDelete = (id) => {
        setIsOpened(true);
        setDeletedItemId(id);
    }

    const onDelete = async () => {
        await dispatch(deleteLesson(deletedItemId));
        setIsOpened(false);
    };

    useEffect(() => {
        dispatch(getLessons()).then(() => {
            setIsLoad(false);
        });
    }, [dispatch]);

    if (isLoad) {
        return <CircularProgress />;
    }

    return (
        <div style={{ width: "100%" }}>
            <Table
                columns={[
                    {
                        title: 'Класс',
                        field: 'classData.name',
                    },
                    {
                        title: 'Преподаватель',
                        field: 'teacher',
                        render: rowData =>
                            `${rowData.teacher.surname} ${rowData.teacher.name[0]}. ${rowData.teacher.patronymic[0]}.` },
                    { title: 'Предмет', field: 'subject.name' },
                    { title: 'Часов в неделю', field: 'countPerWeek' },
                ]}
                options={{
                    filtering: true,
                    actionsColumnIndex: -1,
                }}
                data={lessons}
                actions={user.role === USER_ROLE.ADMIN ? [
                    {
                        icon: () => <EditOutlined />,
                        tooltip: 'Редактировать урок',
                        onClick: (event, rowData) => {
                            const path = generatePath(routeNames.LESSONS.EDIT, {
                                id: rowData.id,
                            });
                            history.push(path);
                        }
                    },
                    {
                        icon: () => <DeleteOutline />,
                        tooltip: 'Удалить урок',
                        onClick: (event, rowData) => handleDelete(rowData.id),
                    },
                    {
                        icon: () => <Add />,
                        tooltip: 'Добавить урок',
                        isFreeAction: true,
                        onClick: () => history.push(routeNames.LESSONS.ADD),
                    }
                ] : []}
            />
            <Dialog
                isOpened={isOpened}
                handleClose={() => setIsOpened(false)}
                handleAdditionalButton={() => setIsOpened(false)}
                handlePrimaryButton={onDelete}
                primaryButtonText="Удалить"
                additionalButtonText="Отмена"
                variant="error"
            >
                <Typography variant="body1">Вы действительно хотите удалить урок?</Typography>
            </Dialog>
        </div>
    )
};

export default Lessons;
