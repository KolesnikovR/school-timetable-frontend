import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Typography } from '@material-ui/core';
import { createLesson } from '../../../actions/lessons';
import LessonForm from '../components/Form';
import routeNames from '../../../consts/routeNames';
import { USER_ROLE } from '../../../consts';

const AddLesson = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.authentication);

    if (user.role !== USER_ROLE.ADMIN) {
        history.push(routeNames.LESSONS.INDEX);
    }

    const onSubmit = async (data) => {
        dispatch(createLesson(data)).then(() => {
            history.push(routeNames.LESSONS.INDEX);
        });
    }

    return (
        <div>
            <Typography variant="h4">Добавить новый урок</Typography>
            <LessonForm btnText="Добавить" onSubmit={onSubmit} />
        </div>
    )
};

export default AddLesson;
