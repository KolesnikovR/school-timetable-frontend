import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CircularProgress, Typography, IconButton } from '@material-ui/core';
import { DeleteOutline, Add, EditOutlined } from '@material-ui/icons';
import { getSubjects, deleteSubject, createSubject, editSubject } from '../../actions/subjects';
import Dialog from '../../components/Dialog';
import Button from '../../components/Button';
import { USER_ROLE } from '../../consts';
import SubjectForm from './components/Form';
import { useSubjectStyles } from './styles';

const Subjects = () => {
    const classes = useSubjectStyles();
    const dispatch = useDispatch();
    const { subjects } = useSelector(state => state.subjectData);
    const { user } = useSelector(state => state.authentication);
    const [isLoad, setIsLoad] = useState(true);
    const [isOpened, setIsOpened] = useState(false);
    const [deletedItemId, setDeletedItemId] = useState(null);
    const [editedItem, setEditedItem] = useState(null);
    const [isAddBtnClicked, setIsAddBtnClicked] = useState(false);

    const onCancel = () => {
        setIsAddBtnClicked(false);
        setEditedItem(null);
    }
    
    const handleDelete = (id) => {
        setIsOpened(true);
        setDeletedItemId(id);
    }

    const onDelete = async () => {
        await dispatch(deleteSubject(deletedItemId));
        setIsOpened(false);
    };

    const onAdd = async (data) => {
        await dispatch(createSubject(data));
        onCancel();
    };

    const onEdit = async (data) => {
        let updatedData = {};
        Object.keys(data).forEach(key => {
            if (data[key] !== editedItem[key]) {
                return updatedData[key] = data[key];
            }
        });
    
        await dispatch(editSubject(editedItem.id, updatedData));
        onCancel();
    };

    useEffect(() => {
        dispatch(getSubjects()).then(() => {
            setIsLoad(false);
        });
    }, [dispatch]);

    return (
        <div style={{ width: "100%" }}>
            {isLoad ? (
                <CircularProgress />
            ) : (
                <div>
                    {user.role === USER_ROLE.ADMIN && (
                        <Button
                            variant="contained"
                            color="primary"
                            size="large"
                            disabled={editedItem || isAddBtnClicked}
                            onClick={() => setIsAddBtnClicked(true)}
                        >
                            <Add />&nbsp;
                            Добавить предмет
                        </Button>
                    )}
                    <div className={classes.list}>
                        {isAddBtnClicked && (
                            <div className={classes.item}>
                                <SubjectForm btnText="Добавить" onSubmit={onAdd} onCancel={onCancel} />
                            </div>
                        )}
                        {subjects.map(subject => (
                            <div className={classes.item} key={subject.id}>
                                {editedItem?.id === subject.id ? (
                                    <SubjectForm
                                        subject={subject}
                                        btnText="Изменить"
                                        onSubmit={onEdit}
                                        onCancel={onCancel}
                                    />
                                ) : (
                                    <>
                                        <div className={classes.itemHeading}>
                                            <Typography variant="h6" style={{ flex: '1 1 100%' }}>{subject.name}</Typography>
                                            {user.role === USER_ROLE.ADMIN && (
                                                <div style={{ flex: '1 0 100px' }}>
                                                    <IconButton
                                                        disabled={editedItem || isAddBtnClicked}
                                                        onClick={() => setEditedItem(subject)}
                                                    >
                                                        <EditOutlined />
                                                    </IconButton>
                                                    <IconButton
                                                        disabled={editedItem || isAddBtnClicked}
                                                        onClick={() => handleDelete(subject.id)}
                                                    >
                                                        <DeleteOutline />
                                                    </IconButton>
                                                </div>
                                            )}
                                        </div>
                                        <Typography variant="body1" className={classes.itemSubHeading}>
                                            Сложность: {subject.difficulty}
                                        </Typography>
                                        <Typography variant="body1" className={classes.itemSubHeading}>
                                            Преподаватели:
                                        </Typography>
                                        <Typography variant="body2">
                                            {subject.teachers.length
                                                ? subject
                                                    .teachers
                                                        .map(it => `${it.surname} ${it.name[0]}. ${it.patronymic[0]}.`)
                                                        .join(', ')
                                                : '—'
                                            }
                                        </Typography>
                                    </>
                                )}
                            </div>
                        ))}
                    </div>
                </div>
            )}
            <Dialog
                isOpened={isOpened}
                handleClose={() => setIsOpened(false)}
                handleAdditionalButton={() => setIsOpened(false)}
                handlePrimaryButton={onDelete}
                primaryButtonText="Удалить"
                additionalButtonText="Отмена"
                variant="error"
            >
                <Typography variant="body1">Вы действительно хотите удалить предмет?</Typography>
            </Dialog>
        </div>
    )
};

export default Subjects;
