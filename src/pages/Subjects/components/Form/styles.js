import { makeStyles } from '@material-ui/core/styles';

export const useFormStyles = makeStyles(theme => ({
    root: {
        "& .MuiFormControl-root": {
            maxWidth: '100%',
            margin: theme.spacing(0, 0, 2),
        },

        "& .MuiButton-root": {
            marginRight: theme.spacing(1),
        }
    },
}));
