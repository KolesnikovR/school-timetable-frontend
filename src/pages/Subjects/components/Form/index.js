import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { CircularProgress } from '@material-ui/core';
import { FormTextField } from '../../../../components/inputs/TextField';
import { FormSelectField } from '../../../../components/inputs/Select';
import Button from '../../../../components/Button';
import { getUsers } from '../../../../actions/user';
import { useFormStyles } from './styles';
import { schema } from './schema';

const SubjectForm = ({ subject, btnText, onSubmit, onCancel }) => {
    const classes = useFormStyles();
    const dispatch = useDispatch();
    const { users } = useSelector(state => state.userData);
    const [isLoaded, setIsLoaded] = useState(false);
    const [isSended, setIsSended] = useState(true);
    const { handleSubmit, formState: { errors }, control } = useForm({
        resolver: yupResolver(schema),
    });
    
    useEffect(() => {
        dispatch(getUsers()).then(() => {
            setIsLoaded(true);
        });
    }, [dispatch]);

    const submitForm = async (data) => {
        setIsSended(false);
        onSubmit(data).then(() => {
            setIsSended(true);
        });
    };

    if (!isLoaded) {
        return <CircularProgress />;
    }

    return (
        <form onSubmit={handleSubmit(submitForm)} className={classes.root}>
            <FormTextField
                label="Название"
                control={control}
                errors={errors}
                name="name"
                defaultValue={subject?.name}
            />
            <FormTextField
                label="Сложность"
                control={control}
                errors={errors}
                name="difficulty"
                defaultValue={subject?.difficulty}
            />
            <FormSelectField
                control={control}
                errors={errors}
                name="teachers"
                label="Преподаватели"
                data={users.map(it => ({ id: it.id, value: `${it.surname} ${it.name[0]}. ${it.patronymic[0]}.` }))}
                defaultValue={subject?.teachers.map(it => it.id) || []}
                multiple
            />
            <Button
                type="submit"
                variant="contained"
                color="primary"
                size="medium"
                disabled={!isSended}
                isLoading={!isSended}
            >
                {btnText}
            </Button>
            <Button
                size="medium"
                onClick={onCancel}
                disabled={!isSended}
            >
                Отмена
            </Button>
        </form>
    );
};

export default SubjectForm;
