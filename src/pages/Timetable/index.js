import React, { useState, useEffect, useCallback } from 'react';
import { useAsyncEffect } from 'use-async-effect';
import { useDispatch, useSelector } from 'react-redux';
import { CircularProgress, Typography } from '@material-ui/core'
import { getUsers } from '../../actions/user';
import { getLessons } from '../../actions/lessons';
import { getSubjects } from '../../actions/subjects';
import { getTime } from '../../shared/api/rest/time';
import { solveTimetable, getTimetable } from '../../shared/api/rest/timetable';
import { DayOfWeek, getLocaleNameForDay } from '../../consts/days';
import { USER_ROLE } from '../../consts';
import Button from '../../components/Button';
import { useStyles } from './styles';

const Timetable = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const { users } = useSelector(state => state.userData);
    const { lessons } = useSelector(state => state.lessonsData);
    const { user } = useSelector(state => state.authentication);
    const [time, setTime] = useState([]);
    const [timetable, setTimetable] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    useAsyncEffect(async () => {
        await dispatch(getUsers());
        await dispatch(getLessons());
        await dispatch(getSubjects());
        const timeData = (await getTime()).data;
        setTime(timeData);
        setIsLoaded(true);
    }, [dispatch]);

    const getTimeout = useCallback(() => {    
        setTimeout(() => {
            getTimetable().then(({ data }) => {
                setTimetable(data);

                if (data?.solverStatus === "SOLVING_ACTIVE") {
                    getTimeout();
                }
            });
        }, 3000);
    }, []);

    useEffect(() => {
        getTimeout();
    }, [getTimeout]);

    const generateTimeTable = async () => {
        let lessonsData = [];
        lessons.forEach(lesson => {
            lessonsData.push(...(new Array(lesson.countPerWeek)).fill(true).map(() => ({
                lessonId: lesson.id,
                subject: lesson.subject.id,
                difficulty: lesson.subject.difficulty,
                teacher: lesson.teacher.id,
                studentGroup: lesson.classData.id,
            })))
        });
        await solveTimetable({
            lessons: lessonsData,
            timeslots: time,
        });
        getTimeout();
    };

    if (!isLoaded) {
        return <CircularProgress />;
    }

    return (
        <>
            <div className={classes.btnContainer}>
                {user.role === USER_ROLE.ADMIN && (
                    <Button
                        variant="contained"
                        color="primary"
                        size="medium"
                        onClick={generateTimeTable}
                    >
                        Генерация
                    </Button>
                )}
                {/* <Button
                    size="medium"
                    // disabled={!isSended}
                    // isLoading={!isSended}
                >
                    Сброс
                </Button> */}
            </div>
            <div className={classes.root}>
                <table>
                    <thead>
                        <th></th>
                        {users.map(user => (
                            <th key={user.id}>
                                <Typography variant="body2">
                                    {`${user.surname} ${user.name[0]}. ${user.patronymic[0]}.`}
                                </Typography>
                            </th>
                        ))}
                    </thead>
                    <tbody>
                        {time.map((it, index) => (
                            <tr key={index}>
                                <td>
                                    {`${getLocaleNameForDay(it.dayOfWeek)} ${it.startTime} - ${it.endTime}`}
                                </td>
                                {users.map(user => (
                                    <td key={user.id}>
                                        {
                                            (() => {
                                                const data = timetable?.lessonList.find(lesson => 
                                                    lesson.teacher === user.id && (
                                                        DayOfWeek[lesson.timeslot.dayOfWeek] === it.dayOfWeek &&
                                                        lesson.timeslot.startTime === `${it.startTime}:00` &&
                                                        lesson.timeslot.endTime === `${it.endTime}:00`
                                                    )
                                                );

                                                if (data) {
                                                    const lesson = lessons.find(it => it.id === data.lessonId);
                                                    lesson.isFinded = true;
                                                    return (
                                                        <div className={classes.lesson}>
                                                            <div className={classes.lessonInner}>
                                                                <Typography variant="body2">
                                                                    {lesson.subject.name}
                                                                </Typography>
                                                                <p className={classes.lessonInnerText}>
                                                                    <em>{lesson.classData.name}</em>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    );
                                                }
                                            })()
                                        }
                                    </td>
                                ))}
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
            <Typography variant="h6">Нераспределенные занятия</Typography>
            <div className={classes.unassignedContainer}>
                {lessons.filter(it => !it.isFinded).map(lesson => (
                    <>
                        {(new Array(lesson.countPerWeek)).fill(true).map(() => (
                            <div className={classes.lesson}>
                                <div className={classes.lessonInner}>
                                    <Typography variant="body2">{lesson.subject.name}</Typography>
                                    <p className={classes.lessonInnerText}><em>{lesson.classData.name}</em></p>
                                </div>
                            </div>
                        ))}
                    </>
                ))}
            </div>
        </>
    );
};

export default Timetable;