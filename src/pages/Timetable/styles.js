import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
    root: {
        '& table': {
            width: '100%',
            marginBottom: '1rem',
            color: '#212529',
            borderCollapse: 'collapse',
            position: 'relative',
        },

        '& th, & td': {
            padding: '0.75rem',
            minWidth: '175px',
        },

        '& th': {
            verticalAlign: 'bottom',
            position: 'sticky',
            top: 0,
        },

        '& tr:nth-of-type(odd)': {
            backgroundColor: 'rgba(0, 0, 0, .05)',
        },

        '& td': {
            verticalAlign: 'middle',
        },
    },

    btnContainer: {
        display: 'flex',

        '& .MuiButton-root': {
            marginRight: theme.spacing(2),
        },
    },

    unassignedContainer: {
        display: 'grid',
        gridTemplateColumns: 'repeat(5, 1fr)',
        gridGap: theme.spacing(1),
    },

    lesson: {
        position: 'relative',
        display: 'flex',
        flexDirection: 'column',
        wordWrap: 'break-word',
        minWidth: '150px',
        backgroundColor: '#fff',
        backgroundClip: 'border-box',
        border: '1px solid rgba(0, 0, 0, .125)',
        borderRadius: '.25rem',
    },

    lessonInner: {
        flex: '1 1 auto',
        padding: theme.spacing(1),
    },

    lessonInnerText: {
        margin: '.5em 0',
    }
}));
