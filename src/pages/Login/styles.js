import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {
        width: "100vw",
        height: "100vh",
    },
    card: {
        margin: "0 auto",
        width: "25rem",
        maxWidth: "90%",
        padding: "2rem",
        boxSizing: "border-box",
    },
    head: {
        marginBottom: "1rem"
    },
    item: {
        marginBottom: "2rem",
    }
});
