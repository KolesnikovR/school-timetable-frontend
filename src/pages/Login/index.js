import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Card, Grid, Typography } from '@material-ui/core';
import { loginAction } from '../../actions/authentication';
import TextField from '../../components/inputs/TextField';
import Button from '../../components/Button';
import routeNames from '../../consts/routeNames';
import { useStyles } from './styles';

const Login = () => {
    const [inputs, setInputs] = useState({
        email: '',
        password: ''
    });
    const [isSubmitted, setIsSubmitted] = useState(false);
    const { email, password } = inputs;
    const classes = useStyles();
    const dispatch = useDispatch();
    const authentication = useSelector(state => state.authentication);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setInputs(inputs => ({ ...inputs, [name]: value }));
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        setIsSubmitted(true);

        if (email && password) {
            await dispatch(loginAction(email, password));
        }

        setIsSubmitted(false);
    }

    if (authentication.loggedIn) {
        return <Redirect to={routeNames.HOME} />
    }

    return (
        <Grid className={classes.root} container direction="column" justify="center" alignItems="center">
            <Card className={classes.card}>
                <Typography className={classes.head} variant="h4" align="center">Вход</Typography>
                <form autoComplete="off" onSubmit={handleSubmit}>
                    <Grid container direction="column" justify="center">
                        <Grid item className={classes.item}>
                            <TextField
                                name="email"
                                required
                                type="email"
                                label="Email"
                                onChange={handleChange}
                            />
                        </Grid>
                        <Grid item className={classes.item}>
                            <TextField
                                name="password"
                                required
                                type="password"
                                label="Пароль"
                                onChange={handleChange}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        size="large"
                        isLoading={isSubmitted}
                    >
                        Войти
                    </Button>
                </form>
            </Card>
        </Grid>
    )
}

export default Login;
