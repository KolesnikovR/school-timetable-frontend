import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Box, Typography, Grid, CircularProgress } from '@material-ui/core';
import { FormTextField, FormPhoneField } from '../../../../components/inputs/TextField';
import { FormSelectField } from '../../../../components/inputs/Select';
import Button from '../../../../components/Button';
import { getSubjects } from '../../../../actions/subjects';
import { useFormStyles } from './styles';
import { schema } from './schema';

const UserForm = ({ user, btnText, onSubmit }) => {
    const classes = useFormStyles();
    const dispatch = useDispatch();
    const { subjects } = useSelector(state => state.subjectData);
    const { user: currentUser } = useSelector(state => state.authentication);
    const [isLoaded, setIsLoaded] = useState(false);
    const [isSended, setIsSended] = useState(true);
    const { handleSubmit, formState: { errors }, control } = useForm({
        resolver: yupResolver(schema),
    });
    
    useEffect(() => {
        dispatch(getSubjects()).then(() => {
            setIsLoaded(true);
        });
    }, [dispatch]);

    const submitForm = async (data) => {
        setIsSended(false);
        onSubmit(data).then(() => {
            setIsSended(true);
        });
    };

    if (!isLoaded) {
        return <CircularProgress />;
    }

    return (
        <form onSubmit={handleSubmit(submitForm)}>
            <Grid container spacing={2} justify="space-between" className={classes.root}>
                <Grid item md={6}>
                    <Box className={classes.section}>
                        <Typography variant="h5">
                            ФИО
                        </Typography>
                        <FormTextField
                            label="Фамилия"
                            control={control}
                            errors={errors}
                            name="surname"
                            defaultValue={user?.surname}
                        />
                        <FormTextField
                            label="Имя"
                            control={control}
                            errors={errors}
                            name="name"
                            defaultValue={user?.name}
                        />
                        <FormTextField
                            label="Отчество"
                            control={control}
                            errors={errors}
                            name="patronymic"
                            defaultValue={user?.patronymic}
                        />
                    </Box>
                </Grid>
                <Grid item md={6}>
                    <Box className={classes.section}>
                        <Typography variant="h5">
                            Контактная информация
                        </Typography>
                        <FormTextField
                            type="email"
                            label="Почта"
                            control={control}
                            errors={errors}
                            name="email"
                            defaultValue={user?.email}
                        />
                        <FormPhoneField
                            label="Номер телефона"
                            control={control}
                            errors={errors}
                            name="phone"
                            defaultValue={user?.phone}
                        />
                    </Box>
                </Grid>
                {user?.id === currentUser.id && (
                    <Grid item xs={12}>
                        <Box className={classes.section}>
                            <Typography variant="h5">
                                Пароль
                            </Typography>
                            <FormTextField
                                style={{ maxWidth: '49%' }}
                                type="password"
                                label="Новый пароль"
                                control={control}
                                errors={errors}
                                name="password"
                            />
                        </Box>
                    </Grid>
                )}
                <Grid item xs={12}>
                    <Box className={classes.section}>
                        <Typography variant="h5">
                            Учебная информация
                        </Typography>
                        <FormSelectField
                            control={control}
                            errors={errors}
                            name="subjects"
                            label="Предметы"
                            data={subjects.map(it => ({ id: it.id, value: it.name }))}
                            defaultValue={user?.subjects.map(it => it.id) || []}
                            multiple
                        />
                    </Box>
                </Grid>
                <Grid item xs={12}>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        size="large"
                        disabled={!isSended}
                        isLoading={!isSended}
                    >
                        {btnText}
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default UserForm;
