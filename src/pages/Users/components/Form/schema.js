import * as yup from 'yup';

export const schema = yup.object().shape({
    name: yup.string().required(),
    surname: yup.string().required(),
    patronymic: yup.string().required(),
    // phone: yup.string().required(),
    email: yup.string().required().email(),
});