import React, { useState } from 'react';
import { useAsyncEffect } from 'use-async-effect';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useParams } from 'react-router';
import { CircularProgress, Typography } from '@material-ui/core';
import { editUser } from '../../../actions/user';
import UserForm from '../components/Form';
import routeNames from '../../../consts/routeNames';
import { USER_ROLE } from '../../../consts/index';
import { getUserById } from '../../../shared/api/rest/users';

const EditUser = () => {
    const history = useHistory();
    let { id: userId } = useParams();
    const dispatch = useDispatch();
    const { user: currentUser } = useSelector(state => state.authentication);
    const [user, setUser] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);

    if (currentUser.role !== USER_ROLE.ADMIN && currentUser.id !== userId) {
        history.push(routeNames.USERS.INDEX);
    }

    useAsyncEffect(async () => {
        getUserById(userId).then(({ data }) => {
            setUser(data);
            setIsLoaded(true);
        });
    }, []);

    const onSubmit = async (data) => {
        let updatedData = {};
        Object.keys(data).forEach(key => {
            if (key === 'password' && data[key]) {
                return updatedData[key] = data[key];
            }

            if (data[key] !== user[key]) {
                return updatedData[key] = data[key];
            }
        });
        
        dispatch(editUser(userId, updatedData)).then(() => {
            history.push(routeNames.USERS.INDEX);
        });
    };

    if (!isLoaded) {
        return <CircularProgress />;
    }

    return (
        <div>
            <Typography variant="h4">Редактировать преподавателя</Typography>
            <UserForm btnText="Редактировать" onSubmit={onSubmit} user={user} />
        </div>
    )
};

export default EditUser;
