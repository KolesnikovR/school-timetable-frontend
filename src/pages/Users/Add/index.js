import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { Typography } from '@material-ui/core';
import { createUser } from '../../../actions/user';
import UserForm from '../components/Form';
import routeNames from '../../../consts/routeNames';
import { USER_ROLE } from '../../../consts';

const AddUser = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { user } = useSelector(state => state.authentication);

    if (user.role !== USER_ROLE.ADMIN) {
        history.push(routeNames.USERS.INDEX);
    }

    const onSubmit = async (data) => {
        dispatch(createUser(data)).then((newData) => {
            history.push(routeNames.USERS.INDEX);
        });
    }

    return (
        <div>
            <Typography variant="h4">Добавить нового преподавателя</Typography>
            <UserForm btnText="Добавить" onSubmit={onSubmit} />
        </div>
    )
};

export default AddUser;
