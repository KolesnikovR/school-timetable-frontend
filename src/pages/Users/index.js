import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { generatePath, useHistory } from 'react-router';
import { CircularProgress, Typography } from '@material-ui/core';
import { DeleteOutline, Add, EditOutlined } from '@material-ui/icons';
import { getUsers, deleteUser } from '../../actions/user';
import Table from '../../components/Table';
import Dialog from '../../components/Dialog';
import routeNames from '../../consts/routeNames';
import { USER_ROLE } from '../../consts';

const Users = () => {
    const history = useHistory();
    const dispatch = useDispatch();
    const { users } = useSelector(state => state.userData);
    const { user } = useSelector(state => state.authentication);
    const [isLoad, setIsLoad] = useState(true);
    const [isOpened, setIsOpened] = useState(false);
    const [deletedItemId, setDeletedItemId] = useState(false);
    
    const handleDelete = (id) => {
        setIsOpened(true);
        setDeletedItemId(id);
    }

    const onDelete = async () => {
        await dispatch(deleteUser(deletedItemId));
        setIsOpened(false);
    };

    useEffect(() => {
        dispatch(getUsers()).then(() => {
            setIsLoad(false);
        });
    }, [dispatch]);

    return (
        <div style={{ width: "100%" }}>
            {isLoad ? (
                <CircularProgress />
            ) : (
                <Table
                    columns={[
                        { title: 'Фамилия', field: 'surname' },
                        { title: 'Имя', field: 'name' },
                        { title: 'Отчество', field: 'patronymic' },
                        { title: 'Почта', field: 'email' },
                        { title: 'Телефон', field: 'phone' },
                    ]}
                    options={{
                        filtering: true,
                        actionsColumnIndex: -1,
                    }}
                    data={users}
                    actions={user.role === USER_ROLE.ADMIN ? [
                        {
                            icon: () => <EditOutlined />,
                            tooltip: 'Редактировать пользователя',
                            onClick: (event, rowData) => {
                                const path = generatePath(routeNames.USERS.EDIT, {
                                    id: rowData.id,
                                });
                                history.push(path);
                            }
                        },
                        {
                            icon: () => <DeleteOutline />,
                            tooltip: 'Удалить пользователя',
                            onClick: (event, rowData) => handleDelete(rowData.id),
                        },
                        {
                            icon: () => <Add />,
                            tooltip: 'Добавить пользователя',
                            isFreeAction: true,
                            onClick: (event) => history.push(routeNames.USERS.ADD),
                        }
                    ] : []}
                />
            )}
            <Dialog
                isOpened={isOpened}
                handleClose={() => setIsOpened(false)}
                handleAdditionalButton={() => setIsOpened(false)}
                handlePrimaryButton={onDelete}
                primaryButtonText="Удалить"
                additionalButtonText="Отмена"
                variant="error"
            >
                <Typography variant="body1">Вы действительно хотите удалить пользователя?</Typography>
            </Dialog>
        </div>
    )
};

export default Users;
