import { Provider } from 'react-redux';
import store from './store';
import AppInner from './components/AppInner';

function App() {
    return (
        <Provider store={store}>
            <AppInner />
        </Provider>
    );
}

export default App;
