import axios from 'axios';
import { APP_API_URL, GENERATOR_API_URL } from '../../consts';

export const httpGet = (url, config) => {
    return getResponse(url, "GET", config);
}

export const httpPost = (url, data, config) => {
    return getResponse(url, "POST", config, data);
}

export const httpPut = (url, data, config) => {
    return getResponse(url, "PUT", config, data);
}

export const httpPatch = (url, data, config) => {
    return getResponse(url, "PATCH", config, data);
}

export const httpDelete = (url, config) => {
    return getResponse(url, "DELETE", config);
}

export const httpGetGenerator = (url, config) => {
    return getResponseFromGeneratorApp(url, "GET", config);
}

export const httpPostGenerator = (url, data, config) => {
    return getResponseFromGeneratorApp(url, "POST", config, data);
}

const getResponse = async (url, method, config, data) => {
    const axiosRequestConfig = {
        ...config,
        url: `${APP_API_URL}${url}`,
        method,
        data,
    };

    const response = await axios(axiosRequestConfig);
    return response;
}

const getResponseFromGeneratorApp = async (url, method, config, data) => {
    const axiosRequestConfig = {
        ...config,
        url: `${GENERATOR_API_URL}${url}`,
        method,
        data,
    };

    const response = await axios(axiosRequestConfig);
    return response;
}
