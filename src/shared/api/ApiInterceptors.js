import React, { useMemo } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import ErrorAlert from '../../components/ErrorAlert';
import { showError, closeError } from '../../actions/error';

export const ApiInterceptors = ({ children }) => {
    const error = useSelector(state => state.error);
    const dispatch = useDispatch();

    useMemo(() => {
        return axios.interceptors.response.use(
            response => response,
            ({ response }) => {
                dispatch(showError({ message: response?.data?.message }));
            }
        );
    }, [dispatch]);

    return (
        <>
            <ErrorAlert isOpen={error.message} message={error.message} onClose={() => dispatch(closeError())} />
            {children}
        </>
    );
}