import { httpGet } from "../apiHandler";

export const getTime = () => {
    return httpGet('time/');
};
