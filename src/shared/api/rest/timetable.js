import { httpPostGenerator, httpGetGenerator } from '../apiHandler';

export const solveTimetable = (data) => {
    return httpPostGenerator('timeTable/', data);
};

export const getTimetable = (data) => {
    return httpGetGenerator('timeTable/', data);
};