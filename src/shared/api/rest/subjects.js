import { httpGet, httpPost, httpPut, httpDelete } from "../apiHandler";
import { getAuthHeader } from '../auth';

export const getAllSubjects = () => {
    return httpGet('subjects/');
};

export const addSubject = (subject) => {
    return httpPost('subjects/', subject, {
        ...getAuthHeader(),
    });
};

export const editSubject = (id, subject) => {
    return httpPut(`subjects/${id}`, subject, {
        ...getAuthHeader(),
    });
};

export const removeSubject = (id) => {
    return httpDelete(`subjects/${id}`, {
        ...getAuthHeader(),
    });
};