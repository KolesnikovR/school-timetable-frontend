import { httpGet, httpDelete, httpPost, httpPut } from "../apiHandler";
import { getAuthHeader } from '../auth';

export const getAllUsers = () => {
    return httpGet('users/');
};

export const getUserById = (id) => {
    return httpGet(`users/${id}`);
};

export const addUser = (user) => {
    return httpPost('auth/register/', user, {
        ...getAuthHeader(),
    });
};

export const editUser = (id, user) => {
    return httpPut(`users/${id}`, user, {
        ...getAuthHeader(),
    });
};

export const removeUser = (id) => {
    return httpDelete(`users/${id}`, {
        ...getAuthHeader(),
    });
};
