import { httpGet, httpPost, httpPut, httpDelete } from "../apiHandler";
import { getAuthHeader } from '../auth';

export const getAllClasses = () => {
    return httpGet('classes/');
};

export const addClass = (subject) => {
    return httpPost('classes/', subject, {
        ...getAuthHeader(),
    });
};

export const editClass = (id, subject) => {
    return httpPut(`classes/${id}`, subject, {
        ...getAuthHeader(),
    });
};

export const removeClass = (id) => {
    return httpDelete(`classes/${id}`, {
        ...getAuthHeader(),
    });
};
