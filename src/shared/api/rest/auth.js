import { httpPost } from "../apiHandler";

export const login = (email, password) => {
    return httpPost('auth/login/', { email, password });
}
