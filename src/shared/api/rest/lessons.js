import { httpGet, httpPost, httpPut, httpDelete } from '../apiHandler';
import { getAuthHeader } from '../auth';

export const getAllLessons = () => {
    return httpGet('lessons/');
};

export const getLessonById = (id) => {
    return httpGet(`lessons/${id}`);
};

export const addLesson = (lesson) => {
    return httpPost('lessons/', lesson, {
        ...getAuthHeader(),
    });
};

export const editLesson = (id, subject) => {
    return httpPut(`lessons/${id}`, subject, {
        ...getAuthHeader(),
    });
};

export const removeLesson = (id) => {
    return httpDelete(`lessons/${id}`, {
        ...getAuthHeader(),
    });
};