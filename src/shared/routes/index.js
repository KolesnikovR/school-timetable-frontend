import { lazy } from 'react';
import routeNames from '../../consts/routeNames';

const Home = lazy(() => import('../../pages/Home'));
const Login = lazy(() => import('../../pages/Login'));
const Users = lazy(() => import('../../pages/Users'));
const AddUser = lazy(() => import('../../pages/Users/Add'));
const EditUser = lazy(() => import('../../pages/Users/Edit'));
const Subjects = lazy(() => import('../../pages/Subjects'));
const Classes = lazy(() => import('../../pages/Classes'));
const Timetable = lazy(() => import('../../pages/Timetable'));
const Lessons = lazy(() => import('../../pages/Lessons'));
const AddLesson = lazy(() => import('../../pages/Lessons/Add'));
const EditLesson = lazy(() => import('../../pages/Lessons/Edit'));

const routes = [
    {
        name: routeNames.LOGIN,
        path: routeNames.LOGIN,
        isPrivate: false,
        component: Login,
    },
    {
        name: routeNames.HOME,
        path: routeNames.HOME,
        isPrivate: true,
        component: Home,
    },
    {
        name: routeNames.USERS.INDEX,
        path: routeNames.USERS.INDEX,
        isPrivate: true,
        component: Users,
        exact: true,
    },
    {
        name: routeNames.USERS.ADD,
        path: routeNames.USERS.ADD,
        isPrivate: true,
        component: AddUser,
        exact: true,
    },
    {
        name: routeNames.USERS.EDIT,
        path: routeNames.USERS.EDIT,
        isPrivate: true,
        component: EditUser,
        exact: true,
    },
    {
        name: routeNames.SUBJECTS,
        path: routeNames.SUBJECTS,
        isPrivate: true,
        component: Subjects,
        exact: true,
    },
    {
        name: routeNames.CLASSES,
        path: routeNames.CLASSES,
        isPrivate: true,
        component: Classes,
        exact: true,
    },
    {
        name: routeNames.LESSONS.INDEX,
        path: routeNames.LESSONS.INDEX,
        isPrivate: true,
        component: Lessons,
        exact: true,
    },
    {
        name: routeNames.LESSONS.ADD,
        path: routeNames.LESSONS.ADD,
        isPrivate: true,
        component: AddLesson,
        exact: true,
    },
    {
        name: routeNames.LESSONS.EDIT,
        path: routeNames.LESSONS.EDIT,
        isPrivate: true,
        component: EditLesson,
        exact: true,
    },
    {
        name: routeNames.TIMETABLE,
        path: routeNames.TIMETABLE,
        isPrivate: true,
        component: Timetable,
        exact: true,
    },
]

export default routes;
