const path = {
    LOGIN: '/login',
    HOME: '/home',
    USERS: {
        INDEX: '/users',
        ADD: '/users/add',
        ITEM: '/users/:id',
        EDIT: '/users/:id/edit'
    },
    SUBJECTS: '/subjects',
    CLASSES: '/classes',
    LESSONS: {
        INDEX: '/lessons',
        ADD: '/lessons/add',
        EDIT: '/lessons/:id/edit'
    },
    TIMETABLE: '/timetable',
};

export default path;