export const APP_API_URL = process.env.NODE_ENV === 'development' ? process.env.REACT_APP_API_URL : window.API_URL;
export const GENERATOR_API_URL = process.env.NODE_ENV === 'development' ? process.env.REACT_APP_GENERATOR_API_URL : window.GENERATOR_API_URL;

export const USER_ROLE = {
    ADMIN: 1,
    TEACHER: 2,
};
