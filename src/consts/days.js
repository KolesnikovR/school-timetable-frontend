export const DayOfWeek = {
    MONDAY: 0,
    TUESDAY: 1,
    WEDNESDAY: 2,
    THURSDAY: 3,
    FRIDAY: 4,
};

export const getLocaleNameForDay = (day) => {
    switch (day) {
        case DayOfWeek.MONDAY: return 'Понедельник';
        case DayOfWeek.TUESDAY: return 'Вторник';
        case DayOfWeek.WEDNESDAY: return 'Среда';
        case DayOfWeek.THURSDAY: return 'Четврег';
        case DayOfWeek.FRIDAY: return 'Пятница';
        default: return null;
    }
}
