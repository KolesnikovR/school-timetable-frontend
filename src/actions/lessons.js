import {
    GET_ALL_LESSONS,
    REMOVE_LESSON,
    ADD_LESSON,
    EDIT_LESSON,
} from '../types/lessons';
import {
    getAllLessons as getAllLessonsApi,
    addLesson as addLessonApi,
    editLesson as editLessonApi,
    removeLesson as removeLessonApi,
} from '../shared/api/rest/lessons';

const getAllLessons = (lessons) => ({ type: GET_ALL_LESSONS, lessons });
const removeLesson = (id) => ({ type: REMOVE_LESSON, id });
const addLesson = (lesson) => ({ type: ADD_LESSON, lesson });
const changeLesson = (lesson) => ({ type: EDIT_LESSON, lesson });

export const getLessons = () => {
    return async (dispatch) => {
        const result = await getAllLessonsApi();

        if (result?.data) {
            dispatch(getAllLessons(result.data));
        }
    }
};

export const createLesson = (lesson) => {
    return async (dispatch) => {
        console.log(lesson);
        const result = await addLessonApi(lesson);

        if (result?.data) {
            dispatch(addLesson(result.data));
        }
    }
}

export const editLesson = (id, subject) => {
    return async (dispatch) => {
        const result = await editLessonApi(id, subject);

        if (result?.data) {
            dispatch(changeLesson(result.data));
        }
    }
}

export const deleteLesson = (id) => {
    return async (dispatch) => {
        await removeLessonApi(id);
        dispatch(removeLesson(id));
    }
};
