import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT,
} from '../types/user';
import { login } from '../shared/api/rest/auth';
import jwtDecode from 'jwt-decode';

const request = (user) => ({ type: LOGIN_REQUEST, user });
const success = (data) => ({ type: LOGIN_SUCCESS, data });
const failure = () => ({ type: LOGIN_FAILURE });
const logout = () => ({ type: LOGOUT });

export const loginAction = (email, password) => {
    return async (dispatch) => {
        dispatch(request({ email }));
        const result = await login(email, password);

        if (result?.data?.success) {
            const user = jwtDecode(result.data.token.accessToken);
            localStorage.setItem('user', JSON.stringify(user));
            localStorage.setItem('token', result.data.token.accessToken);
            dispatch(success(user));
        } else {
            dispatch(failure());
        }
    }
}

export const logoutAction = () => {
    return async (dispatch) => {
        localStorage.removeItem('user');
        dispatch(logout());
    }
}
