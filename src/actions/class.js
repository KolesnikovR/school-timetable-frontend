import {
    ADD_CLASS,
    EDIT_CLASS,
    GET_ALL_CLASSES,
    REMOVE_CLASS,
} from '../types/classes';
import {
    getAllClasses as getAllClassesApi,
    addClass as addClassApi,
    editClass as editClassApi,
    removeClass as removeClassApi,
} from '../shared/api/rest/classes';

const getAllClasses = (classes) => ({ type: GET_ALL_CLASSES, classes });
const removeClass = (id) => ({ type: REMOVE_CLASS, id });
const addClass = (classData) => ({ type: ADD_CLASS, class: classData });
const changeClass = (classData) => ({ type: EDIT_CLASS, class: classData });

export const getClasses = () => {
    return async (dispatch) => {
        const result = await getAllClassesApi();

        if (result?.data) {
            dispatch(getAllClasses(result.data));
        }
    }
};

export const createClass = (classData) => {
    return async (dispatch) => {
        const result = await addClassApi(classData);

        if (result?.data) {
            dispatch(addClass(result.data));
        }
    }
}

export const editClass = (id, classData) => {
    return async (dispatch) => {
        const result = await editClassApi(id, classData);

        if (result?.data) {
            dispatch(changeClass(result.data));
        }
    }
}

export const deleteClass = (id) => {
    return async (dispatch) => {
        await removeClassApi(id);
        dispatch(removeClass(id));
    }
};
