import {
    SHOW_ERROR,
    CLOSE_ERROR,
} from '../types/error';

export const showError = ({ message }) => ({ type: SHOW_ERROR, message });

export const closeError = () => ({ type: CLOSE_ERROR });
