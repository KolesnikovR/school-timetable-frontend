import {
    GET_ALL_USERS,
    REMOVE_USER,
    ADD_USER,
    EDIT_USER,
} from '../types/user';
import {
    getAllUsers as getAllUsersApi,
    addUser as addUserApi,
    editUser as editUserApi,
    removeUser as removeUserApi,
} from '../shared/api/rest/users';

const getAllUsers = (users) => ({ type: GET_ALL_USERS, users });
const removeUser = (id) => ({ type: REMOVE_USER, id });
const addUser = (user) => ({ type: ADD_USER, user });
const changeUser = (user) => ({ type: EDIT_USER, user });

export const getUsers = () => {
    return async (dispatch) => {
        const result = await getAllUsersApi();

        if (result?.data) {
            dispatch(getAllUsers(result.data));
        }
    }
}

export const createUser = (user) => {
    return async (dispatch) => {
        const result = await addUserApi(user);

        if (result?.data) {
            dispatch(addUser(result.data));
        }
    }
}

export const editUser = (id, user) => {
    return async (dispatch) => {
        const result = await editUserApi(id, user);

        if (result?.data) {
            dispatch(changeUser(result.data));
        }
    }
}

export const deleteUser = (id) => {
    return async (dispatch) => {
        await removeUserApi(id);
        dispatch(removeUser(id));
    }
}
