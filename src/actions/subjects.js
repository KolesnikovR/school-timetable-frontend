import {
    ADD_SUBJECT,
    EDIT_SUBJECT,
    GET_ALL_SUBJECTS,
    REMOVE_SUBJECT,
} from '../types/subjects';
import {
    getAllSubjects as getAllSubjectsApi,
    addSubject as addSubjectApi,
    editSubject as editSubjectApi,
    removeSubject as removeSubjectApi,
} from '../shared/api/rest/subjects';

const getAllSubjects = (subjects) => ({ type: GET_ALL_SUBJECTS, subjects });
const removeSubject = (id) => ({ type: REMOVE_SUBJECT, id });
const addSubject = (subject) => ({ type: ADD_SUBJECT, subject });
const changeSubject = (subject) => ({ type: EDIT_SUBJECT, subject });

export const getSubjects = () => {
    return async (dispatch) => {
        const result = await getAllSubjectsApi();

        if (result?.data) {
            dispatch(getAllSubjects(result.data));
        }
    }
};

export const createSubject = (subject) => {
    return async (dispatch) => {
        const result = await addSubjectApi(subject);

        if (result?.data) {
            dispatch(addSubject(result.data));
        }
    }
}

export const editSubject = (id, subject) => {
    return async (dispatch) => {
        const result = await editSubjectApi(id, subject);

        if (result?.data) {
            dispatch(changeSubject(result.data));
        }
    }
}

export const deleteSubject = (id) => {
    return async (dispatch) => {
        await removeSubjectApi(id);
        dispatch(removeSubject(id));
    }
};
