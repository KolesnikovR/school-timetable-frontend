import React from 'react';
import { Route } from 'react-router-dom';
import BasicPublicLayout from '../../layouts/BasicPublicLayout';

const PublicRoute = ({ component, ...rest }) => {
    const simpleRouteElement = (props) => {
        return <BasicPublicLayout>{React.createElement(component, props)}</BasicPublicLayout>;
    };

    return <Route {...rest} render={simpleRouteElement} />
}

export default PublicRoute;
