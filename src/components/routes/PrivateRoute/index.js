import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import routeNames from '../../../consts/routeNames';
import BasicProtectedLayout from '../../layouts/BasicProtectedLayout';

const PrivateRoute = ({ component: Component, roles, ...rest }) => {
    const { user } = useSelector(state => state.authentication);
    const hasAccess = user && (!roles || roles.includes(user.role));

    return (
        <Route {...rest} render={props => {
            if (!hasAccess) {
                return <Redirect to={routeNames.LOGIN} />
            }

            return (
                <BasicProtectedLayout>
                    <Component {...props} />
                </BasicProtectedLayout>
            );
        }} />
    );
}

export default PrivateRoute;
