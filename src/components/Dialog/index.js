import React, { useMemo } from "react";
import {
    Box,
    Button,
    Dialog as MaterialDialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Divider,
    IconButton,
    Typography,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";
import {
    useBackdropStyles,
    useButtonCloseStyles,
    useDialogActionsStyles,
    useDialogContentStyles,
    useDialogStyles,
    useDialogTitleStyles,
    useDividerStyles,
} from './styles';

const Dialog = ({
    children,
    dialogTitle,
    isOpened = false,
    showCloseIcon = true,
    variant = "default",
    primaryButtonText,
    additionalButtonText,
    buttonPosition,
    handleClose,
    handleAdditionalButton,
    handlePrimaryButton,
}) => {
    const backdropStyles = useBackdropStyles();
    const dialogStyles = useDialogStyles();
    const dividerStyles = useDividerStyles();
    const dialogTitleStyles = useDialogTitleStyles();
    const dialogContentStyles = useDialogContentStyles();
    const dialogActionsStyles = useDialogActionsStyles();
    const buttonCloseStyles = useButtonCloseStyles();

    const groupButtons = useMemo(() => {
        const palette = variant === "default" ? "primary" : "secondary";
        const primaryButton = (
            <Button variant="contained" color={palette} onClick={handlePrimaryButton} type="button">
                {primaryButtonText}
            </Button>
        );

        const additionalButton = additionalButtonText ? (
            <Button variant="contained" color="primary" onClick={handleAdditionalButton}>
                {additionalButtonText}
            </Button>
        ) : null;

        return variant === "default" ? (
            <>
                {additionalButton}
                {primaryButton}
            </>
        ) : (
            <>
                {primaryButton}
                {additionalButton}
            </>
        );
    }, [
        variant,
        primaryButtonText,
        handlePrimaryButton,
        additionalButtonText,
        handleAdditionalButton,
    ]);

    let content = (
        <>
            <DialogContent classes={{ ...dialogContentStyles }}>{children}</DialogContent>
            <Divider classes={{ ...dividerStyles }} />
            <DialogActions
                classes={{ ...dialogActionsStyles }}
                className={buttonPosition === "start" ? dialogStyles.startButtons : ""}
            >
                {groupButtons}
            </DialogActions>
        </>
    );

    return (
        <MaterialDialog
            onClose={handleClose}
            open={isOpened}
            classes={{
                paper: dialogStyles.paper,
                scrollPaper: dialogStyles.scrollPaper,
                paperScrollPaper: dialogStyles.paperScrollPaper,
            }}
            BackdropProps={{ className: backdropStyles.backdrop }}
            disableBackdropClick
        >
            <DialogTitle classes={{ ...dialogTitleStyles }} disableTypography>
                <Typography variant="h4">
                    {dialogTitle}
                    {showCloseIcon && (
                        <IconButton onClick={handleClose} classes={{ ...buttonCloseStyles }}>
                            <Close />
                        </IconButton>
                    )}
                </Typography>
            </DialogTitle>
            <Box className={dialogStyles.content}>{content}</Box>
        </MaterialDialog>
    );
};

export default Dialog;