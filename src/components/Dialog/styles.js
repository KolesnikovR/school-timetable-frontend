import { makeStyles } from '@material-ui/core/styles';

export const useBackdropStyles = makeStyles(() => ({
    backdrop: {
        opacity: "0.4 !important",
    },
}));

export const useDialogStyles = makeStyles(theme => ({
    paper: {
        flex: "1 1 auto",
        maxWidth: "680px",
        borderRadius: "8px 8px 0px 0px",
        margin: "auto 0 0 0",
        [theme.breakpoints.up("sm")]: {
            margin: 0,
            top: 80,
            borderRadius: theme.spacing(0.5),
        },
        [theme.breakpoints.down("sm")]: {
            margin: "auto 0 0 0",
            top: 0,
        },
    },
    content: {
        overflowY: "auto",
    },
    scrollPaper: {
        alignItems: "flex-start",
    },
    paperScrollPaper: {
        maxHeight: "calc(100% - 16px)",
    },
    startButtons: {
        justifyContent: "start",
    },
    optionalButtonBox: {
        marginRight: "auto",
        [theme.breakpoints.down("xs")]: {
            marginRight: 0,
            margin: `${theme.spacing(1.5)}px 0 ${theme.spacing(1)}px 0`,
        },
    },
}));

export const useDialogTitleStyles = makeStyles(theme => ({
    root: {
        padding: `${theme.spacing(3)}px ${theme.spacing(3)}px ${theme.spacing(2)}px  ${theme.spacing(4)}px`,
        [theme.breakpoints.down("xs")]: {
            padding: `${theme.spacing(2)}px ${theme.spacing(2)}px ${theme.spacing(2)}px  ${theme.spacing(2)}px`,
        },
    },
}));

export const useDialogContentStyles = makeStyles(theme => ({
    root: {
        flex: "1 0 auto",
        wordBreak: "break-word",
        padding: theme.spacing(0, 3, 0, 4),
        [theme.breakpoints.down("xs")]: {
            padding: theme.spacing(0, 2, 0, 2),
        },
        "&:first-child": {
            paddingTop: 0,
        },
    },
}));

export const useDialogActionsStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(0, 3, 3, 4),
        [theme.breakpoints.down("xs")]: {
            padding: theme.spacing(0, 3, 5, 3),
            flexDirection: "column",
            alignItems: "center",
        },
    },
    spacing: {
        [theme.breakpoints.down("xs")]: {
            "& :not(:first-child)": {
                marginLeft: 0,
                marginTop: theme.spacing(2),
            },
        },
    },
}));

export const useButtonCloseStyles = makeStyles(() => ({
    root: {
        padding: 0,
        position: "absolute",
        right: "24px",
        "& svg": {
            fontSize: "24px",
        },
    },
}));

export const useDividerStyles = makeStyles(theme => ({
    root: {
        height: "1px",
        margin: theme.spacing(4, 0, 3),
        [theme.breakpoints.down("xs")]: {
            margin: theme.spacing(4, 0, 3, 0),
        },
    },
}));
