import React from 'react';
import { get } from 'lodash';
import { Typography } from '@material-ui/core';
import { ErrorMessage } from '@hookform/error-message';

const FormError = ({ errors, name }) => (
    <ErrorMessage
        as={<Typography variant="caption" className="roleErrorMessage" role="errorMessage" />}
        errors={errors}
        name={name}
    />
);

export const shouldShowFormError = ({ errors, name }) => !!get(errors, name, '');

export default FormError;
