import React from 'react';
import { FormControl, InputLabel, Select as MaterialSelect, MenuItem, Chip, Input } from '@material-ui/core';
import { Controller } from 'react-hook-form';
import FormError, { shouldShowFormError } from '../FormError';
import { useStyles } from './styles';

export const Select = ({ multiple, data, label, ...props }) => {
    const classes = useStyles();
    const ITEM_HEIGHT = 48;
    const ITEM_PADDING_TOP = 8;

    const MenuProps = {
        PaperProps: {
            style: {
                maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
                width: 250,
            },
        },
    };

    return (
        <FormControl className={classes.formControl}>
            <InputLabel>{label}</InputLabel>
            <MaterialSelect
                multiple={multiple}
                input={<Input id="select-multiple-chip" />}
                renderValue={(selected) => {
                    if (multiple) {
                        return (
                            <div className={classes.chips}>
                                {selected.map((value) => (
                                    <Chip
                                        key={value}
                                        className={classes.chip}
                                        label={data.find(it => it.id === value).value}
                                    />
                                ))}
                            </div>
                        );
                    }
                    return data.find(it => it.id === selected).value;
                }}
                {...props}
                MenuProps={MenuProps}
            >
                {data.map(({ id, value }) => (
                    <MenuItem key={id} value={id}>
                        {value}
                    </MenuItem>
                ))}
            </MaterialSelect>
        </FormControl>
    );
};

export const FormSelectField = ({ control, errors, ...props }) => {
    const { name, defaultValue } = props;

    return (
        <Controller
            render={({ field }) => (
                <Select
                    helperText={<FormError errors={errors} name={name} />}
                    showError={shouldShowFormError({ name, errors })}
                    {...field}
                    {...props}
                />
            )}
            name={name}
            control={control}
            defaultValue={defaultValue}
        />
    );
};
