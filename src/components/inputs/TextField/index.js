import React from 'react';
import { Controller } from 'react-hook-form';
import { TextField as MaterialTextField } from '@material-ui/core';
import MuiPhoneNumber from 'material-ui-phone-number';
import FormError, { shouldShowFormError } from '../FormError';
import { useStyles } from './styles';

const TextField = ({ ...rest }) => {
    const classes = useStyles();

    return (
        <MaterialTextField {...rest} className={classes.root} />
    );
};

export const FormTextField = ({ control, errors, defaultValue = "", ...props }) => {
    const { name } = props;
    return (
        <Controller
            render={({ field }) => (
                <TextField
                    helperText={<FormError errors={errors} name={name} />}
                    showError={shouldShowFormError({ name, errors })}
                    {...props}
                    {...field}
                />
            )}
            name={name}
            control={control}
            defaultValue={defaultValue}
        />
    );
};

export const FormPhoneField = ({ control, errors, ...props }) => {
    const { name, defaultValue } = props;
    const classes = useStyles();

    return (
        <Controller
            render={({ field }) => (
                <MuiPhoneNumber
                    {...field}
                    onChange={(...event) => {
                        const newEvent = event;
                        newEvent[0] = newEvent[0].replace(/[\s)(-]/g, "");
                        field.onChange(...newEvent);
                    }}
                    defaultCountry="by"
                    autoFormat
                    showError={shouldShowFormError({ name, errors })}
                    helperText={<FormError errors={errors} name={name} />}
                    className={classes.root}
                    {...props}
                />
            )}
            name={name}
            control={control}
            defaultValue={defaultValue}
        />
    );
};

export default TextField;