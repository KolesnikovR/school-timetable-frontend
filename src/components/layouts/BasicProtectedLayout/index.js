import React, { useRef, useState } from "react";
import { Grid, AppBar, IconButton, Toolbar, Hidden } from "@material-ui/core";
import { Menu } from "@material-ui/icons";
import { useStyles } from "./styles";
import Nav from '../../Nav';

const BasicProtectedLayout = ({ children }) => {
    const classes = useStyles();
    const [isOpened, setIsOpened] = useState(false);
    const ref = useRef();

    const onClose = (e) => {
        if (e.target === ref.current) {
            setIsOpened(false);
        }
    }

    return (
        <div ref={ref} onClick={onClose} className={`${classes.root} ${isOpened ? 'menu-opened' : ''}`}>
            <Hidden lgUp>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="open drawer"
                            edge="start"
                            onClick={() => setIsOpened(!isOpened)}
                            className={classes.menuButton}
                        >
                            <Menu />
                        </IconButton>
                    </Toolbar>
                </AppBar>
            </Hidden>
            <Grid container wrap="nowrap">
                <Grid item className={`${classes.nav} ${isOpened ? 'opened' : ''}`}>
                    <Nav />
                </Grid>
                <Grid item className={classes.body}>{children}</Grid>
            </Grid>
        </div>
    );
};

export default BasicProtectedLayout;