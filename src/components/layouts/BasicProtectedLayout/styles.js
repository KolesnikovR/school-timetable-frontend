import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    root: {
        minHeight: "100vh",
        display: "flex",

        [theme.breakpoints.down("md")]: {
            "&.menu-opened": {
                position: "relative",
            },
    
            "&.menu-opened::after": {
                content: '""',
                position: "absolute",
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                background: "rgba(0, 0, 0, .3)",
                zIndex: 99999,
            }
        },
    },
    nav: {
        [theme.breakpoints.up("md")]: {
            maxWidth: "340px",
        },

        [theme.breakpoints.down("md")]: {
            maxWidth: "250px",
            position: "fixed",
            top: 0,
            left: "-100%",
            bottom: 0,
            zIndex: 999999,
            transition: "left .5s",

            "&.opened": {
                left: 0,
            }
        },
    },
    body: {
        padding: "2rem",
        flex: "1 1 100%",
        maxWidth: "100%",
        overflow: "auto",

        [theme.breakpoints.down("md")]: {
            marginTop: "64px",
            flex: "0 0 100%",
        },
    }
}));
