import React from "react";
import { useStyles } from "./styles";

const BasicPublicLayout = ({ children }) => {
    const classes = useStyles();

    return <div className={classes.root}>{children}</div>;
};

export default BasicPublicLayout;