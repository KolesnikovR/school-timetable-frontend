import React from 'react';
import { Snackbar, IconButton } from '@material-ui/core';
import MuiAlert from '@material-ui/lab/Alert';
import CloseIcon from '@material-ui/icons/Close';

const Alert = (props) => {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

const ErrorAlert = ({ message, isOpen, onClose }) => (
    <Snackbar
        anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
        }}
        open={isOpen}
        onClose={onClose}
        autoHideDuration={4000}
        action={
            <IconButton size="small" aria-label="close" color="inherit" onClick={onClose}>
                <CloseIcon fontSize="small" />
            </IconButton>
        }
    >
        <Alert severity="error">{message}</Alert>
    </Snackbar>
)

export default ErrorAlert;