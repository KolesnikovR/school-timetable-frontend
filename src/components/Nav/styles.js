import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {
        paddingTop: "1rem",
        boxSizing: "border-box",
    },

    fullHeight: {
        height: "100%",
        minHeight: "100vh",
    },

    navMenu: {
        flex: "1 0 auto",
    },

    userInfo: {
        padding: "1rem",
    },

    avatar: {
        width: "4rem",
        height: "4rem",
    },

    username: {
        lineHeight: "1.5rem",
    },

    menu: {
        paddingTop: "1rem",
    },

    zIndex: {
        zIndex: 9999999,
    },
});
