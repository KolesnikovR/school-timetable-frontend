import React, { useRef, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useLocation, generatePath } from 'react-router-dom'
import {
    Grid,
    Paper,
    Avatar,
    Typography,
    IconButton,
    Popper,
    ClickAwayListener,
    MenuItem,
    MenuList,
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
} from '@material-ui/core';
import { ArrowDropDown, School, Book, Face, Schedule, Apps } from '@material-ui/icons';
import { logoutAction } from '../../actions/authentication';
import routeNames from '../../consts/routeNames';
import { useStyles } from './styles';

const drawer = (classes, pathname) => (
    <div>
        <div className={classes.toolbar} />
        <List>
            <ListItem
                button
                component="a"
                href={routeNames.USERS.INDEX}
                selected={
                    pathname.startsWith(routeNames.USERS.INDEX) ||
                    pathname.startsWith(`/${routeNames.USERS.INDEX}`)
                }
            >
                <ListItemIcon>
                    <Face />
                </ListItemIcon>
                <ListItemText primary="Преподаватели" />
            </ListItem>
            <ListItem
                button
                component="a"
                href={routeNames.SUBJECTS}
                selected={
                    pathname.startsWith(routeNames.SUBJECTS) ||
                    pathname.startsWith(`/${routeNames.SUBJECTS}`)
                }
            >
                <ListItemIcon>
                    <Book />
                </ListItemIcon>
                <ListItemText primary="Предметы" />
            </ListItem>
            <ListItem
                button
                component="a"
                href={routeNames.CLASSES}
                selected={
                    pathname.startsWith(routeNames.CLASSES) ||
                    pathname.startsWith(`/${routeNames.CLASSES}`)
                }
            >
                <ListItemIcon>
                    <School />
                </ListItemIcon>
                <ListItemText primary="Классы" />
            </ListItem>
            <ListItem
                button
                component="a"
                href={routeNames.LESSONS.INDEX}
                selected={
                    pathname.startsWith(routeNames.LESSONS.INDEX) ||
                    pathname.startsWith(`/${routeNames.LESSONS.INDEX}`)
                }
            >
                <ListItemIcon>
                    <Apps />
                </ListItemIcon>
                <ListItemText primary="Уроки" />
            </ListItem>
            <ListItem
                button
                component="a"
                href={routeNames.TIMETABLE}
                selected={
                    pathname.startsWith(routeNames.TIMETABLE) ||
                    pathname.startsWith(`/${routeNames.TIMETABLE}`)
                }
            >
                <ListItemIcon>
                    <Schedule />
                </ListItemIcon>
                <ListItemText primary="Расписание" />
            </ListItem>
        </List>
    </div>
);

const Nav = () => {
    const classes = useStyles();
    const { user } = useSelector(state => state.authentication);
    const dispatch = useDispatch();
    const anchorRef = useRef(null);
    const history = useHistory();
    const [isPopupOpened, setIsPopupOpened] = useState(false);
    const { pathname } = useLocation();

    const handleToggle = (e) => {
        setIsPopupOpened((prevOpen) => !prevOpen);
    };

    const handleListKeyDown = (event) => {
        if (event.key === 'Tab') {
            event.preventDefault();
            setIsPopupOpened(false);
        }
    }

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
          return;
        }
    
        setIsPopupOpened(false);
    };

    const prevOpen = React.useRef(isPopupOpened);
    useEffect(() => {
        if (prevOpen.current && !isPopupOpened) {
            anchorRef.current.focus();
        }

        prevOpen.current = isPopupOpened;
    }, [isPopupOpened]);

    const onLogout = async () => {
        await dispatch(logoutAction());
        history.push(routeNames.LOGIN);
    };

    const onProfile = async () => {
        const path = generatePath(routeNames.USERS.EDIT, {
            id: user.id,
        });
        history.push(path);
    }
    
    return (
        <Grid container direction="column" className={classes.fullHeight}>
            <Paper className={classes.fullHeight}>
                <Grid container className={classes.root} direction="column" wrap="nowrap">
                    <Grid
                        item
                        container
                        className={classes.userInfo}
                        alignItems="center"
                        spacing={2}
                        wrap="nowrap"
                    >
                        <Grid item>
                            <Avatar sizes="5" className={classes.avatar} />
                        </Grid>
                        <Grid item>
                            <Typography variant="h6" className={classes.username}>
                                {`${user.surname} ${user.name}`}
                            </Typography>
                        </Grid>
                        <Grid item>
                            <IconButton
                                ref={anchorRef}
                                aria-controls={isPopupOpened ? 'menu-list-grow' : undefined}
                                aria-haspopup="true"
                                onClick={handleToggle}
                            >
                                <ArrowDropDown />
                            </IconButton>
                            <Popper
                                open={isPopupOpened}
                                anchorEl={anchorRef.current}
                                placement="bottom-start"
                                className={classes.zIndex}
                                transition
                                disablePortal
                            >
                                <Paper>
                                    <ClickAwayListener onClickAway={handleClose}>
                                        <MenuList
                                            autoFocusItem={isPopupOpened}
                                            id="menu-list-grow"
                                            onKeyDown={handleListKeyDown}
                                        >
                                            <MenuItem onClick={onProfile}>Профиль</MenuItem>
                                            <MenuItem onClick={onLogout}>Выйти</MenuItem>
                                        </MenuList>
                                    </ClickAwayListener>
                                </Paper>
                            </Popper>
                        </Grid>
                    </Grid>
                    <Grid item className={classes.menu}>
                        <nav className={classes.drawer} aria-label="mailbox folders">
                            {drawer(classes, pathname)}
                        </nav>
                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
}

export default Nav;