import React, { Suspense } from 'react';
import { Switch, Router, Redirect, Route } from 'react-router';
import { createBrowserHistory } from 'history';
import { CircularProgress } from '@material-ui/core';
import { ApiInterceptors } from '../../shared/api/ApiInterceptors';
import routes from '../../shared/routes';
import routeNames from '../../consts/routeNames';
import PublicRoute from '../routes/PublicRoute';
import PrivateRoute from '../routes/PrivateRoute';

const history = createBrowserHistory();

const AppInner = () => {
    return (
        <ApiInterceptors>
            <Suspense fallback={<CircularProgress />}>
                <Router history={history}>
                    <Switch>
                        {routes.map(route =>
                            route.isPrivate ? (
                                <PrivateRoute
                                    key={route.name}
                                    exact={route.exact}
                                    path={route.path}
                                    component={route.component}
                                />) : (
                                <PublicRoute
                                    key={route.name}
                                    exact={route.exact}
                                    path={route.path}
                                    component={route.component}
                                />
                            )
                        )}
                        <Route exact path="/" component={() => <Redirect to={routeNames.HOME} />} />
                    </Switch>
                </Router>
            </Suspense>
        </ApiInterceptors>
    );
}

export default AppInner;
