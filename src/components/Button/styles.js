import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    progress: {
        position: "absolute",
        top: "50%",
        left: "50%",
        margin: "-0.5rem 0 0 -0.5rem",
    },
});
