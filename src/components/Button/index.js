import React from 'react';
import { Button as MaterialButton, CircularProgress } from '@material-ui/core';
import { useStyles } from './styles';

const Button = ({ children, isLoading, ...props }) => {
    const classes = useStyles();

    return (
        <MaterialButton disabled={isLoading} {...props}>
            {isLoading && (
                <CircularProgress className={classes.progress} size="1rem" />
            )}
            {children}
        </MaterialButton>
    )
}

export default Button;