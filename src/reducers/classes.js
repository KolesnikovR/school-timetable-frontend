import {
    GET_ALL_CLASSES,
    REMOVE_CLASS,
    ADD_CLASS,
    EDIT_CLASS,
} from '../types/classes';

const initialState = { classes: [] };

const classes = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_CLASSES:
            return {
                classes: action.classes,
            };
        case REMOVE_CLASS:
            return {
                classes: state.classes.filter(it => it.id !== action.id),
            };
        case ADD_CLASS:
            return {
                classes: [...state.classes, action.class],
            };
        case EDIT_CLASS:
            console.log(action.class.id);
            return {
                classes: state.classes.map(it => {
                    return action.class.id === it.id ? action.class : it;
                }),
            };
        default:
            return state;
    }
}

export default classes;
