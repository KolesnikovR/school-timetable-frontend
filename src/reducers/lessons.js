import {
    GET_ALL_LESSONS,
    REMOVE_LESSON,
    ADD_LESSON,
    EDIT_LESSON,
} from '../types/lessons';

const initialState = { lessons: [] };

const lessons = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_LESSONS:
            return {
                lessons: action.lessons,
            };
        case REMOVE_LESSON:
            return {
                lessons: state.lessons.filter(it => it.id !== action.id),
            };
        case ADD_LESSON:
            return {
                lessons: [...state.lessons, action.lesson],
            };
        case EDIT_LESSON:
            return {
                lessons: state.lessons.map(it => {
                    return action.lesson.id === it.id ? action.lesson : it;
                }),
            };
        default:
            return state;
    }
}

export default lessons;
