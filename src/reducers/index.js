import { combineReducers } from 'redux';
import authentication from './authentication';
import error from './error';
import userData from './user';
import subjectData from './subjects';
import classData from './classes';
import lessonsData from './lessons';

const rootReducer = combineReducers({
    authentication,
    error,
    userData,
    subjectData,
    classData,
    lessonsData,
});

export default rootReducer;
