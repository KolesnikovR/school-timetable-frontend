import {
    GET_ALL_SUBJECTS,
    REMOVE_SUBJECT,
    ADD_SUBJECT,
    EDIT_SUBJECT,
} from '../types/subjects';

const initialState = { subjects: [] };

const subjects = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_SUBJECTS:
            return {
                subjects: action.subjects,
            };
        case REMOVE_SUBJECT:
            return {
                subjects: state.subjects.filter(it => it.id !== action.id),
            };
        case ADD_SUBJECT:
            return {
                subjects: [...state.subjects, action.subject],
            };
        case EDIT_SUBJECT:
            return {
                subjects: state.subjects.map(it => {
                    return action.subject.id === it.id ? action.subject : it;
                }),
            };
        default:
            return state;
    }
}

export default subjects;
