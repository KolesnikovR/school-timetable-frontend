import {
    GET_ALL_USERS,
    REMOVE_USER,
    ADD_USER,
    EDIT_USER,
} from '../types/user';

const initialState = { users: [] };

const users = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_USERS:
            return {
                users: action.users,
            };
        case REMOVE_USER:
            return {
                users: state.users.filter(it => it.id !== action.id),
            };
        case ADD_USER:
            return {
                users: [...state.users, action.user],
            };
        case EDIT_USER:
            return {
                users: state.users.map(it => {
                    return action.user.id === it.id ? action.user : it;
                }),
            };
        default:
            return state;
    }
}

export default users;
