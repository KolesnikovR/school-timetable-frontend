import {
    SHOW_ERROR,
    CLOSE_ERROR,
} from '../types/error';

const initialState = { message: null };

const error = (state = initialState, action) => {
    switch (action.type) {
        case SHOW_ERROR:
            return {
                message: action.message
            };
        case CLOSE_ERROR:
            return {
                message: null
            };
        default:
            return state;
    }
}

export default error;
