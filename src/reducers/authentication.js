import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_FAILURE,
    LOGOUT,
} from '../types/user';

let user = JSON.parse(localStorage.getItem('user'));
const initialState = user ? { loggedIn: true, user } : {};

const authentication = (state = initialState, action) => {
    switch (action.type) {
        case LOGIN_REQUEST:
            return {
                loggingIn: false,
                user: action.user
            };
        case LOGIN_SUCCESS:
            return {
                loggedIn: true,
                user: action.data
            };
        case LOGIN_FAILURE:
            return {
                loggedIn: false,
                user: null
            };
        case LOGOUT:
            return {
                loggedIn: false,
                user: null
            };
        default:
            return state;
    }
}

export default authentication;
