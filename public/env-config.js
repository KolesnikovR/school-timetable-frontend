window.APP_SETTINGS = {
    clientId: "undefined",
    clientSecret: "undefined",
    authApiBaseUri: "undefined",
    staticCmsApiBaseUri: "undefined",
    fileServiceApiBaseUri: "undefined",
    timeStoneApiBaseUri: "undefined",
    spaceStoneApiBaseUri: "undefined",
    refreshTokenLifeTimeSeconds: undefined,
    localizationCacheDurationSeconds: undefined,
};